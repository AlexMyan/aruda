require 'spec_helper'

RSpec.describe 'First Run', :type => :aruba do

  let(:file) { 'file.txt' }
  let(:content) { 'Hello, Aruba!' }
  let(:package_lock) { 'package-lock.json'}

  before(:each) { touch(package_lock) }
  # before(:each) { write_file file, content }
  # before(:each) { run_command('aruba-test-cli file.txt') }

  # Full string
  # it { expect(last_command_started).to have_output content }

  # Substring
  # it { expect(last_command_started).to have_output(/Hello/) }

  context 'aruba examples' do
    it { expect(package_lock).to be_an_existing_file }
    it { expect(file).not_to be_an_existing_file }
  end

  context 'is the root directory of the project' do

    it { expect(File.file?(package_lock)).to be(true) }
  end
end

