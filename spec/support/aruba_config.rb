require 'spec_helper'


Aruba.configure do |config|

  Dir.chdir(File.expand_path(File.join(config.root_directory, config.working_directory))) { Dir.entries(".") }
  Dir.glob(File.expand_path(File.join(config.root_directory, config.working_directory)+"/*", __FILE__)).each do |file|
    file
  end

  config.home_directory = File.join(config.root_directory,
                                    config.working_directory,
                                    "lib_root")
  config.root_directory

  config.before(:each) do
    set_env 'HOME', File.expand_path(File.join(aruba.current_directory, 'home'))
    FileUtils.mkdir_p ENV['HOME']
  end

  puts %(The default home_dir "#{config.home_directory}")
  print 'root_dir'
  print config.root_directory
  puts ' '
  puts %(The default value working_dir "#{config.working_directory}")
end